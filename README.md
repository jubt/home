# 聚BT - 聚合最优质的BT、磁力资源   
### 请Ctrl+D收藏本网页地址，确保永久访问  

地址发布页：[https://1jubt.top](https://1jubt.top)  或  [https://1jubt.vip](https://1jubt.vip)   或  [https://1jubt.xyz](https://1jubt.xyz) 


| 地址       | 类型  | 是否需要翻墙 |  
| :---       |     :---:      |          ---: |    
| [jubt5.one](https://jubt5.one)    | 最新地址 | 不需要 |   
| [jubt4.one](https://jubt4.one)    | 最新地址 | 不需要 |  
| [jubt3.one](https://jubt3.one)    | 最新地址 | 不需要 |  
| [jubt2.one](https://jubt2.one)    | 最新地址 | 不需要 | 
| [jubt1.one](https://jubt1.one)    | 最新地址 | 不需要 |
| [jubt13.xyz](https://jubt13.xyz)    | 最新地址 | 不需要 |      
| [jubt12.xyz](https://jubt12.xyz)    | 最新地址 | 不需要 |
| [jubt11.xyz](https://jubt11.xyz)    | 最新地址 | 不需要 |   
| [jubt2-10.xyz](https://jubt10.xyz)    | 最新地址 | 不需要 |   
| [jubt1.xyz](https://jubt1.xyz)    | 最新地址 | 不需要 |         
| [jubt.top](https://jubt.top) | 永久地址 | 不需要 |  
 

永久：永久官方地址，会自动跳转到最新地址

最新：最新可访问地址，建议使用 


聚BT论坛： 

[https://bbs.jubt5.one](https://bbs.jubt5.one)  

[https://bbs.jubt4.one](https://bbs.jubt4.one)  

[https://bbs.jubt3.one](https://bbs.jubt3.one)  

[https://bbs.jubt2.one](https://bbs.jubt2.one)  

[https://bbs.jubt1.one](https://bbs.jubt1.one)  

[https://bbs.jubt13.xyz](https://bbs.jubt13.xyz) 

[https://bbs.jubt12.xyz](https://bbs.jubt12.xyz)  

[https://bbs.jubt11.xyz](https://bbs.jubt11.xyz)  

[https://bbs.jubt2-10.xyz](https://bbs.jubt10.xyz)  

[https://bbs.jubt1.xyz](https://bbs.jubt1.xyz)  

[https://bbs.jubt.top](https://bbs.jubt.top)    


支持邮箱：[support@jubt.net](mailto:support@jubt.net)  

Telegram频道：[@i1fuli](https://t.me/i1fuli)  





  






